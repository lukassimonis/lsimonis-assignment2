MarkSYS Test Project 002
========================

### Installation Steps:
1. fork the repository
2. checkout forked repository to your local machine
3. run `composer install` (Composer must be installed. More details [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx))
4. set database parameters in `app/config/parameters.yml`
5. run application. Details [here](http://symfony.com/doc/current/setup.html#running-your-symfony-application)
6. more info on installing and running Symfony application [here](http://symfony.com/doc/current/setup.html)

### Application Details:
This application is a simple CSV files database. CSV files can be uploaded and viewed.
Access the index page at `/` path. Once a CSV file is submitted, it is being persisted
into MySQL database. Access files at the main page. Content of individual CSV file is
accessible at this path - `/view/{uuid}`, where `{uuid}` is the UUID of the file. At
this time there is no pagination, the whole file will be rendered. Large files will
cause request timeout error. Use relatively small CSV files for this assignment.

### Assignment
Dummy headers and content are displayed for each file. Implement logic to display the
actual file content.

### Extra
1. As the added value, you could add pagination to the file display page (not required).
2. Another potential improvement is to support large files upload (not required).

### Submission
Once done, send us a link to your repository containing the completed assignment. Specify
branch, if different from master. Describe what you have done with the assignment.

### Solution
Note: the "row" property in the csvCell entity causes a problem in MySQL 8.0.2+
(row is a reserved keyword). To resolve, I had to explicitly name the property
"`row`". This should be backwards compatible, but if there are any issues running
on older MySQL versions please try reverting this change.

Main Assignment:
Removed logic from the csvFile entity that generated the dummy data. Added the 
findHeaderRow and findColumnRows functions to the CsvFileRepository entity
repository. Updated the getFile function to set appropriate attributes of CsvFile
objects.
Deleted now-uneccessary static data file.

Pagination:
Implemented paginator helper function and called it from the findColumnRows
function (both in CsvRepository )to get chunks of rows based on currentPage 
and limit (set in controller).
Updated viewFileAction function in DefaultController to take page number parameter
and pass limit, rowCount, thisPage, thisUuid to the twig template.
Updated view.html.twig to add pagination controls to bottom of page.


Large Files:
Implemented batch loading by modifying the uploadAction function in the 
DefaultController. Now it commits changes and flushes the entity manager
after every 10 rows (set with $batchsize variable).

Note: Large files will still fail immediately if they are over the upload_max_filesize
set in php.ini. I had to update this on my local machine.