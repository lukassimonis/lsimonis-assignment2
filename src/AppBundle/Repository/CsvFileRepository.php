<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CsvCell;
use AppBundle\Entity\CsvFile;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CsvFileRepository extends EntityRepository
{

    public function getFile($uuid, $page, $limit)
    {
        /** @var CsvFile $csvFile */
        $csvFile = $this->findFile($uuid);

        $this->headerRow = $this->findHeaderRow($csvFile->getId());
        $columnRows = $this->findColumnRows($csvFile->getId(), $limit, $page);

        $csvFile->setHeaderRow($this->headerRow);
        $csvFile->setCellRows($columnRows);
        $csvFile->setRowCount($this->rowCount);

        return $csvFile;
    }

    public function findFile($uuid)
    {
        $dql = "SELECT csv ";
        $dql .= "FROM " . CsvFile::class . " csv ";
        $dql .= "WHERE csv.uuid = :uuid";

        $em = $this->getEntityManager();
        $query = $em->createQuery($dql);
        $query->setParameter('uuid', $uuid);

        $result = $query->getResult();

        if ($result) {
            return $result[0];
        }

        return null;
    }

    /**
    * This function queries the ORM with DQL to return the cells of the
    * first row of the file, which should contain the header information.
    * @param int $id
    * @return CsvCell
    */
    public function findHeaderRow($id)
    {
        $dbql = "SELECT cell ";
        $dbql .= "FROM " . CsvCell::class . " cell ";
        $dbql .= "WHERE cell.csvFile = " . $id . " ";
        $dbql .= "AND cell.row = " . 1 . " ";
        $dbql .= "ORDER BY cell.col";

        $em = $this->getEntityManager();
        $query = $em->createQuery($dbql);


        return $query->getResult();
    }

    /**
    * This function queries the ORM with DQL to return a 2D array of
    * cell values. It calls the paginate helper function to break
    * the results into chunks rather than returning all the data at once
    * @param $id the id of the csv file
    * @param $limit the number of rows to return
    * @param $currentPage
    * @return array
    */
    public function findColumnRows($id, $limit, $currentPage=1)
    {
        $dbql = "SELECT cell ";
        $dbql .= "FROM " . CsvCell::class . " cell ";
        $dbql .= "WHERE cell.csvFile = " . $id . " ";
        $dbql .= "AND cell.row > " . 1 . " ";

        $em = $this->getEntityManager();
        $query = $em->createQuery($dbql);

        $cellLimit = $limit * count($this->headerRow); # rows * number of cells/row
        $result = $this->paginate($query, $currentPage, $cellLimit);

        if (count($this->headerRow) > 0)
        {
            $this->rowCount = $result->count() / count($this->headerRow);
        } else {
            $this->rowCount = 0;
        }
        $rows = array();

        foreach ($result as $cell)
        {
            $rows[$cell->getRow()][$cell->getCol()] = $cell->getValue();
        }

        return $rows;
        
    }

    /** 
    * Helper function for pagination.
    * @param $query DQL query to execute
    * @param $page
    * @param $limit
    * @return Paginator
    */
    public function paginate($query, $page=1, $limit=10)
    {
        $paginator = new Paginator($query);

        $paginator->getQuery()
                  ->setFirstResult($limit * ($page - 1))
                  ->setMaxResults($limit);

        return $paginator;
    }

}