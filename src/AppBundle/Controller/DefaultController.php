<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CsvCell;
use AppBundle\Entity\CsvFile;
use AppBundle\Form\CsvUploadForm;
use AppBundle\Model\CsvUploadModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $csvFiles = $em->getRepository(CsvFile::class)->findAll();

        return $this->render('@App/index.html.twig', [
            'files' => $csvFiles,
        ]);
    }

    /**
    * Added batch processing and disabled SQL logging for performance.
    * @Route("/upload", name="upload_file")
    */
    public function uploadAction(Request $request, LoggerInterface $logger)
    {
        $model = new CsvUploadModel();
        $form = $this->createForm(CsvUploadForm::class, $model);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (($handle = fopen($model->getFile()->getRealPath(), 'r')) !== false) {
                $rowKey = 1;
                $csvFile = new CsvFile();
                $csvFile->setName($model->getFile()->getClientOriginalName());

                $logger->info("file opened");

                $batchsize = 10;
                $em = $this->get('doctrine.orm.default_entity_manager');

                # Disable SQL logging for performance improvement for large files
                $em->getConnection()->getConfiguration()->setSQLLogger(null);

                while (($row = fgetcsv($handle, 1000, ",")) !== false) {

                    foreach ($row as $cellKey => $cellValue) {
                        $csvFile->addCell(CsvCell::createCell($rowKey, $cellKey + 1, $cellValue));
                    }

                    if ($rowKey % $batchsize == 0) #commit / flush after every $batchsize rows.
                    {
                        try {
                        $em->persist($csvFile);
                        $em->flush();
                        } catch (\Exception $exception) {
                            throw new \RuntimeException($exception->getMessage());
                        }
                    }

                    $rowKey++;
                }
                fclose($handle);

                # Get the last rows not yet committed:
                try {
                    $em->persist($csvFile);
                    $em->flush();
                } catch (\Exception $exception) {
                    throw new \RuntimeException($exception->getMessage());
                }
            } else {
                throw new \RuntimeException('could not open file for reading');
            }

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('@App/upload.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/view/{uuid}/{page}", name="view_file")
     * @param int $page passed via url
     */
    public function viewFileAction($uuid, $page=1)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $limit = 5; # rows per page


        /** @var CsvFile $csvFile */
        $csvFile = $em->getRepository(CsvFile::class)->getFile($uuid, $page, $limit);
        if (is_null($csvFile)) {
            throw $this->createNotFoundException();
        }

        return $this->render('@App/view.html.twig', [
            'file' => $csvFile,
            'limit' => $limit,
            'rowCount' => $csvFile->getRowCount(),
            'thisPage' => $page, 
            'thisUuid' => $uuid
        ]);
    }
}
